<?php

require_once 'total.php';

class Vreka implements Total{
	public $cenaVreka;
	public $qtyVreki;

	public function __construct($cenaVreka, $qtyVreki){
		$this->cenaVreka = $cenaVreka;
		$this->qtyVreki = $qtyVreki;
	}

	public function total(){
		$priceBag = $this->cenaVreka * $this->qtyVreki;
		return $priceBag;
	}
}
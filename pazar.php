<?php

class Pazar{
	public $tezga;
	public $tezgaKod;
	public $plod;

	public function __construct($tezga, $tezgaKod, $plod = [])
	{
		$this->tezgaKod = $tezgaKod;
		$this->tezga = $tezga;
		$this->plod = $plod;
	}

	public function zbir($zbirPlod){

		$this->plod[] = $zbirPlod;
	} 
}
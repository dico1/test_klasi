<?php

require_once 'total.php';


class kg implements Total{
	public $cenaKg;
	public $qty;

	public function __construct($cenaKg, $qty)
	{
		$this->cenaKg = $cenaKg;
		$this->qty = $qty;
	}

	public function total(){
		$priceKg = $this->cenaKg * $this->qty;
		return $priceKg;
	}
}